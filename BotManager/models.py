# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
from django.conf import settings
import json
from PaxcelBotFramework.BotManager.watsonBot import WatsonBot
#from DecisionLogicLayer.models import processBotResponse
from PaxcelBotFramework.HelperTools.models import dotDict
from PaxcelBotFramework.BotManager.templateBot import TemplateBot
from templateBotData import TemplateBotData as botTemplates



class BotManager():

    @staticmethod
    def getProcessedNLPBotResponse(botRequest):
        """

        :param inputRequest: a BotRequest json schema
        :return: MessageLayerModelObject that contains the complete processed bot response

        this function gets the bot response and then processes it to create messages

        """
        print botRequest["rawInput"]['data']['workspaceid'],'bheem'
        authParams=dotDict(settings.WATSON_CONVERSATION_PARAMETERS)
        #print botRequest["info"]["workSpaceId"],"aksdhkasjhdjah"
        botManager = WatsonBot(authParams,name=settings.BOT_NAME)
        __BotWorkspaceIDs__ = {
            "AGENT": botRequest["rawInput"]['data']['workspaceid'],
            "TRAVEL": '37e891f4-87d4-46f8-a36b-866a1ad2574a'

        }
        botResponse=botManager.getResponse(botRequest,__BotWorkspaceIDs__) # Calls the watson api

        return botResponse


    @staticmethod
    def getTemplateBotResponse(botRequest):
        """

        :param botRequest:
        :return:
        """
        templateBot = TemplateBot(botTemplate=botTemplates[settings.TEMPLATE_BOT])

        botResponse = templateBot.getResponse(botRequest)

        return botResponse









