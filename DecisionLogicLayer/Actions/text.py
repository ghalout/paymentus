from DecisionLogicLayer.Components.actionManager import ActionManager

def formatText(data, context):
    return data
def agentCall(data, context):
    return data
def options(data, context):
    return data

def otpVerify(data,context):
    otpData = {
        "success":True,
        "user":"Pallavi"

    }
    return otpData
def addToContext(data,context):
    context["$"+data["key"]] = data["value"]
    return context

def CurrentBillUpdate(data, context):
    data["Bill"] = "500$"
    # data["Bill_Date"] = "31-09-2019"
    # data["Service_Address"] = ""
    # data["Bill_Number"] = ""
    # data["Biller_Name"] = ""
    return data

def PreviousBillUpdate(data, context):
    data["Bill"] = "1000$"
    # data["Account_Number"] = ""
    # data["Bill_Date"] = ""
    # data["Service_Address"] = ""
    # data["Bill_Number"] = ""
    # data["Biller_Name"] = ""
    return data

def billanalysisdetails(data, context):
    data["Bill_Analysis"] = ""
    return data


def KnowMyBill(data, context):
    context["KnowMyBill"] = ""
    data["Account_Number"] = "0943207427304734"
    data["Bill_Date"] = "31-09-2019"
    data["Service_Address"] = "NAN"
    data["Bill_Number"] = "875648487"
    data["Biller_Name"] = "Pallavi"

    return data

ActionManager.add_action("text", formatText)
ActionManager.add_action("agentCall", agentCall)
ActionManager.add_action("otpVerify", otpVerify)
ActionManager.add_action("addToContext", addToContext)
ActionManager.add_action("options", options)
ActionManager.add_action("current_bill_summary", CurrentBillUpdate)
ActionManager.add_action("previous_bill_summary", PreviousBillUpdate)
ActionManager.add_action("detail_bill_analysis", billanalysisdetails)
ActionManager.add_action("knowMyBill", KnowMyBill)

