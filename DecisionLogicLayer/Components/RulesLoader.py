from os import listdir
from os.path import isfile, join
import json
from DataAccessLayer.dbHandler import getResponse_Intent,getRuleList
from django.conf import settings
class RulesLoader:
    def __init__(self):
        self.baseDir = "Rules/.."
        self.Rules =[]
        # self.__load__()
    
    def __load__(self):
        dataList = getRuleList(settings.DatabaseName)
        for file in dataList:
            self.Rules.append(file["get_datajsonlist"])







    
    # def getRules(self, intent, parentNodeId):
    #     #     if len(self.Rules) == 0:
    #     #        #self.__load__()
    #     #        print intent,'value'
    #     #        data = getResponse_Intent(intent,settings.DatabaseName)
    #     #
    #     #        self.Rules.append(data)
    #     #     # selectedRules = []
    #     #     # for rule in self.Rules:
    #     #     #     if intent in rule["condition"]:
    #     #     #         selectedRules.append(rule)
    #     #
    #     #     return self.Rules
    def getRules(self, intent, parentNodeId):

        if len(self.Rules) == 0:
            self.__load__()
        selectedRules = []

        for rule in self.Rules:

            if intent in rule["condition"]:
                selectedRules.append(rule)

        return selectedRules

    def getRuleById(self, nodeid):
        if len(self.Rules) == 0:
            self.__load__()

        for rule in self.Rules:

            if rule["nodeid"] == nodeid:

                return rule
        return None

    def HasChildNode(self, parentNodeId):
        if parentNodeId == "MakePayment":
            return True
        return False

    def getAnythingElseRule(self):
        return self.getRules("anything_else", None)



