

def wrapper(watsonResponse):

    dict = {
        "$nodes": [],
        "#intent":None
    }
    for intents in watsonResponse["intents"]:
        if len(intents) > 0:
            dict["#intent"] = intents['intent']
    for entities in watsonResponse['entities']:
        entity = entities['entity']
        if len(entities) > 0:
            dict['@' + str(entity)] = entities['value']
    context = watsonResponse['context']
    for key in context.keys():
        dict['$' + str(key)] = context[key]

    # dict['~custon_info'] = None

    return dict


# wrapper(watsonResponse)