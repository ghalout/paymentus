class ConditionEvaluator:
    def __init__(self):
        self.expressions = None
        self.pos = 0
        self.value = None
        self.Tokens = []

    def __parser__(self, expression):
        
        self.expressions =  expression.split(" ")
        while self.pos < len(self.expressions):
            if self.__isIntent__():
                self.Tokens.append({
                    "value": self.expressions[self.pos][1:],
                    "type": "intent"
                })
            elif self.__isEntity__():
                parts = self.expressions[self.pos].split(":")
                value = parts[0]
                expectedValue = None
                if len(parts) > 1:

                    expectedValue = parts[1]
                self.Tokens.append({
                    "value": value,
                    "type": "entity",
                    "expectedValue": expectedValue
                })
            elif self.__isContext__():

                parts = self.expressions[self.pos].split(":")
                value = parts[0]
                expectedValue = None
                if len(parts) > 1:

                    expectedValue = parts[1]
                self.Tokens.append({
                    "value": value,
                    "type": "context",
                    "expectedValue": expectedValue
                })
            elif self.__isLocal__():
                parts = self.expressions[self.pos].split(":")
                value = None
                expectedValue = None
                if len(parts) > 1:
                    value = parts[0]  
                    expectedValue = parts[1]
                self.Tokens.append({
                    "value": value,
                    "type": "local",
                    "expectedValue": expectedValue
                })

            elif self.__isOperator__():
                self.Tokens.append({
                    "value": self.expressions[self.pos],
                    "type": "operator"
                })
            self.pos += 1
    
    def __eval__(self, values):
        tokens = self.Tokens
        operators= []
        result = True

        for token in tokens:
            if token["type"] == "intent":
                output = token["value"] == values["#intent"]
                if len(operators) > 0:
                    operator = operators.pop()
                    if operator == "and":
                        result = result and output
                    elif operator == "or":
                        result = result or output
                else:
                    result = output
            elif token["type"] == "entity" or token["type"] == "context" or token["type"] == "local":
                output = token["value"] in values
                if output:
                    if token["expectedValue"] == None:
                        output = True
                    else:
                        output = token["expectedValue"] == values[token["value"]]
                
                if len(operators) > 0:
                    operator = operators.pop()
                    if operator == "and":
                        result = result and output
                    elif operator == "or":
                        result = result or output
                else:
                    result = output
            elif token["type"] == "operator":
                operators.append(token["value"])
        
        return result

    def __isEntity__(self):
        return self.expressions[self.pos].startswith("@", 0)

    def __isIntent__(self):
        return self.expressions[self.pos].startswith("#",0)
    
    def __isContext__(self):
        return self.expressions[self.pos].startswith("$", 0)

    def __isLocal__(self):
        return self.expressions[self.pos].startswith("~", 0)

    def __isOperator__(self):
        operator = ["and", "or"]
        return self.expressions[self.pos] in operator
    
    def Eval(self, expression, values):

        self.__parser__(expression)
        return self.__eval__(values)



