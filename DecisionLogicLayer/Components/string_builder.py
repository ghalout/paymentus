def string_builder(stringformat, outputDict):
    words = stringformat.split(" ")
    compiledWords = []
    sentenceEndChar = [",", ".", "!"]
    for word in words:
        filterWord = None
        deliminator = None
        if word[-1] in sentenceEndChar:
            filterWord = word[0:-1]
            deliminator = word[-1]
        else:
            filterWord = word

        if filterWord in outputDict:
            result = outputDict[filterWord]
            if deliminator:
                result += deliminator

            compiledWords.append(str(result))
        else:
            compiledWords.append(str(filterWord))

    string = " ".join(compiledWords)

    return string



