from Components.WatsonResponseWrapper import wrapper
from Components.RulesLoader import RulesLoader
from Components.models import Parser
# from Components.action import requestHandle
from Components.string_builder import string_builder
from Components.actionManager import ActionManager
from Components.conditionEvaluator import ConditionEvaluator
import json
class RuleEngine:
    def __init__(self):
        self.context = {}


    def Execute(self, nlpResponse):
        response = {}
        self.context = wrapper(nlpResponse.copy())

        # Get the selected node
        selectedNode = self.__selectNode__(nlpResponse)
        response = self.__processNode__(selectedNode)
        #end selected node
        return self.setNPLDistinationBotContext(response)


    
    def __processNode__(self, selectedNode):

        self.context["$nodes"].append({
            "nodeid": selectedNode["nodeid"],
            "redirectType": None
        })
        response = {}
        slots = selectedNode["slots"]
        output = self.__processSlots__(slots)
        if not output["success"]:
            self.context["$nodes"][-1]["redirectType"] = "slot"
            response["context"] = self.__prepareContext__()
            response["output"] = output["output"]
            return response
        # end slots execution 

        actions = selectedNode["actions"]
        self.__processActions__(actions)

        responses = selectedNode["response"]
        result = self.__processResponse__(responses)
        if result["action"] == "jump":
            self.context["$nodes"][-1]["redirectType"] = "jump"    
            selectedNode = self.__selectNodeById__(result["nodeid"])
            result = self.__processNode__(selectedNode)
        
        self.context["$nodes"][-1]["redirectType"] = "return"
        response["context"] = self.__prepareContext__()
        response["output"] = result["output"]
        return response
    def __prepareContext__(self):
        context = {}
        for item in self.context.keys():
            if item[0] == "$":
                context[item[1:]] = self.context[item]

        return context

    def __selectNode__(self, nlpResponse):
        parentNodeId = None

        if len(self.context["$nodes"]) > 0:

            lastNode = self.context["$nodes"][-1]
            parentNodeId = lastNode["nodeid"]
            if lastNode["redirectType"] == "slot":
                return self.__selectNodeById__(parentNodeId)

        intent = self.context["#intent"]

        loader = RulesLoader()
        rules = loader.getRules(intent, parentNodeId)
        selectedRule = None
        for rule in rules:
            condition = rule["condition"]

            if self.__evalCondition__(condition):
                selectedRule = rule
                break

        if selectedRule == None:
            selectedRule = loader.getAnythingElseRule()


        return selectedRule
    
    def __selectNodeById__(self, nodeid):
        loader = RulesLoader()
        rule = loader.getRuleById(nodeid)
        return rule

    def __processSlots__(self, slots):
        response = {
            "success": True
        }
        for slot in slots:
            checkFor = slot["checkFor"]
            saveAs = slot["saveAs"]
            if checkFor not in self.context:
                if saveAs not in self.context:
                    if not slot["required"]:
                        self.context[saveAs] = slot["default"]
                    else:
                        response = {
                            "success": False,
                            "output": {
                                "text": string_builder(slot["text"], self.context)
                            }
                        }
                        return response
            else:
                self.context[saveAs] = self.context[checkFor]
        return response
    
    def __processActions__(self, actions):
        for action in actions:

            response = ActionManager.call_action(action["type"],{ 
                "data": action["value"], 
                "context": self.context.copy()
                })
            self.__populateActionResponseInContext__(response)

    def __populateActionResponseInContext__(self, items):
        specialKeyWords = ["#", "$", "~"]
        
        for item in items.keys():
            if item[0] in specialKeyWords:
                self.context[item] = items[item]
            else:
                self.context["~"+item] = items[item]

    def __processResponse__(self, response):
        result = {
            "action": "return",
            "output": {}
        }
        if response["type"] == "text":
            result["output"]["text"] = string_builder( response["text"], self.context)
        elif response["type"] == "conditional":
            conditions = response["conditionalResponse"]
            for item in conditions:
                if self.__evalCondition__(item["condition"]):
                   result = self.__processResponse__(item["response"])
                   break
        elif response["type"] == "jumpto":
            result = {
                "action": "jump",
                "nodeid": response["nodeid"]
            }
        elif response["type"] == "options":
            options = []

            if type(response["Options"]) is str:
                if response["Options"] in self.context:
                    options = self.context[response["Options"]]
                else:
                    options.append(response["Options"])
            
            elif type(response["Options"]) is list:
                options = response["Options"]

            result["output"] = {
                "text":  string_builder( response["text"], self.context),
                "options": options
            }

        return result
    
    def __evalCondition__(self, condition):
        evaluator = ConditionEvaluator()
        return evaluator.Eval(condition, self.context)

    def setNPLDistinationBotContext(self, response):
        lastNode = response["context"]["nodes"][-1]
        lastNodeId = lastNode["nodeid"]
        loader = RulesLoader()
        if not loader.HasChildNode(lastNodeId) and lastNode["redirectType"] != "slot":
            if "destination_bot" in response["context"]:
                del response["context"]["destination_bot"]

        return response