from PaxcelBotFramework.DecisionLogicLayer.models import SolverFactory
from DecisionLogicLayer.sendEmail import sendingUserDetails
from django.conf import settings
import uuid
from PaxcelBotFramework.DataAccessLayer.chatlogsHandler import dbChatlogHandler
import datetime
from DataAccessLayer.dbHandler import getparentid,getNLPResponse

from django.views.decorators.csrf import csrf_exempt
from RuleEngine import RuleEngine
from Actions import *

import json
#

class On():

    def __init__(self, watsonOutput=None,operation=None):


        '''

        This is the input object to the Policy file of Intellect solver
        For more details read some examples in

        https://pypi.python.org/pypi/Intellect

        Solver Object Initialiser

        :param topIntent: the top most intent
        :param operation: function handle (modified by the policy file)



        '''

        self.watsonOutput = watsonOutput
        self.operation=operation

    @property
    def watsonOutput(self):
        return self.watsonOutput


    @watsonOutput.setter
    def watsonOutput(self, value):
        self.watsonOutput = value


    @property
    def operation(self):
        return self.operation


    @operation.setter
    def operation(self, value):
        self.operation = value



class Do():

    """

    This contains a list of all the actions based on the decision made by the rule engine

    each function will have a BotResponse() object (for definition see PaxcelBotFramework.Botmanager.models file) as input
    and will return a MessageLayerModel() object (for definition see PaxcelBotFramework.MessageFormationLayer.models)


    the data access layer may be called as required, and the output is sent to the MessageFormationLayer for
    creation of message.

    """



    @staticmethod
    def handleNLPGreetings(botResponse):
        """
        Action to be performed in case of greetings

        """
        componentTray = []
        #print botResponse, "++++++++++BOTRESPONSE+++++++++++++"

        if 'dbName' in botResponse["botRequest"]["rawInput"]['data']:
            dbName = botResponse["botRequest"]["rawInput"]['data']['dbName']
            botResponse["botRequest"]['info']['dbName'] = dbName

            watsonOutput = botResponse["outputMessages"][-1]["message"]
            output = getNLPResponse(str(watsonOutput),dbName)

            greetingsBasicWithoutName = {
                "data": output,  # from data access layer#print "+++++++++BOTRESPONSE++++++++++", botResponse
                "tag": "createNLPmessage.data",  # this is for message formation layer
                "componentType": "APIDATA",
                "message": "",  #
                "name": str(uuid.uuid4())  # a random name everytime
            }
            componentTray.append(greetingsBasicWithoutName)

        elif 'dbName' in botResponse["botRequest"]['info']:
            dbName = botResponse["botRequest"]["rawInput"]['data']['dbName']

            watsonOutput = botResponse["outputMessages"][-1]["message"]
            output = getNLPResponse(str(watsonOutput),dbName)

            greetingsBasicWithoutName = {
                "data": output,  # from data access layer#print "+++++++++BOTRESPONSE++++++++++", botResponse
                "tag": "createNLPmessage.data",  # this is for message formation layer
                "componentType": "APIDATA",
                "message": "",  #
                "name": str(uuid.uuid4())  # a random name everytime
            }
            componentTray.append(greetingsBasicWithoutName)


        return [botResponse,componentTray]

    @staticmethod
    def GeneralGreetings(botResponse):
        """
        Action to be performed in case of greetings
        """
        componentTray = []
        if 'dbName' in botResponse["botRequest"]["rawInput"]['data']:
            dbName = botResponse["botRequest"]["rawInput"]['data']['dbName']
            botResponse["botRequest"]['info']['dbName'] = dbName

            parentId = botResponse["botRequest"]["rawInput"]["data"]["parentId"]
            result = getparentid(parentId,dbName)

            apiData = {
                "data": result ,  # from data access layer
                "tag": "createData.data",  # this is for message formation layer
                "componentType": "APIDATA",
                "message": "",  #
                "name": str(uuid.uuid4())  # a random name everytime
            }

            componentTray.append(apiData)

        elif 'dbName' in botResponse["botRequest"]['info']:
            dbName = botResponse["botRequest"]['info']['dbName']
            parentId = botResponse["botRequest"]["rawInput"]["data"]["parentId"]
            result = getparentid(parentId, dbName)

            apiData = {
                "data": result,  # from data access layer
                "tag": "createData.data",  # this is for message formation layer
                "componentType": "APIDATA",
                "message": "",  #
                "name": str(uuid.uuid4())  # a random name everytime
            }

            componentTray.append(apiData)


        return [botResponse,componentTray]



def processBotResponse(botManagerResponse):

    """

    Loads the Policy solver and performs policy solution expressions , returns messageLayerResponse object

    :param:  botResponse : A BotResponse() object (for definition see PaxcelBotFramework.Botmanager.models file)
    :return: messageLayerResponse : A MessageLayerModel() object (for definition see PaxcelBotFramework.MessageFormationLayer.models)


    """
    print botManagerResponse,"botmanager response"
    settings.DatabaseName = botManagerResponse["botRequest"]["rawInput"]['data']['dbName']

    #wrapperOutput = wrapper(botManagerResponse)
    # policyFilePath = settings.POLICY_FILE_PATH
    # policySolver =SolverFactory().getSolverInstance(type=settings.SOLVER_TYPE,policyFilePath=policyFilePath)
    # watsonOutput = botManagerResponse["outputMessages"][-1]["message"]
    # print watsonOutput
    # OnObjectUpdated = policySolver.getResponse(On(watsonOutput=watsonOutput))
    # # on object the operation property has been updated
    # [botResponse, componentTray] = OnObjectUpdated.operation(botManagerResponse)
    #
    # uiComponentTrayFromDecisionLayer = {
    #     "botResponse": botResponse,
    #     "uiComponents": componentTray
    #
    # }
    engine = RuleEngine()
    # Pay Bill
    output = engine.Execute(botManagerResponse)
    print output,"rule engine"

    result = output["output"]
    componentTray = []
    botManagerResponse['context'] = output["context"]
    apiData = {
        "data": result,  # from data access layer
        "tag": "createData.data",  # this is for message formation layer
        "componentType": "APIDATA",
        "message": "",  #
        "name": str(uuid.uuid4())  # a random name everytime
    }
    componentTray.append(apiData)
    uiComponentTrayFromDecisionLayer = {
        "botResponse": botManagerResponse,
        "uiComponents": componentTray

    }
    return (uiComponentTrayFromDecisionLayer)










