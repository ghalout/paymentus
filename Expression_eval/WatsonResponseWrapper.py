# watsonResponse = {
#   "intents": [
#     {
#       "intent": "Detect-Action",
#       "confidence": 0.7227152347564698
#     }
#   ],
#   "entities": [
#     {
#       "entity": "action",
#       "location": [
#         16,
#         20
#       ],
#       "value": "view",
#       "confidence": 1
#     }
#   ],
#   "input": {
#     "text": "i would like to view my bills"
#   },
#   "output": {
#     "generic": [
#       {
#         "response_type": "text",
#         "text": "action"
#       }
#     ],
#     "text": [
#       "action"
#     ],
#     "nodes_visited": [
#       "node_1_1558005092998"
#     ],
#     "log_messages": []
#   },
#   "context": {
#       "billerID" : 123,
#     "conversation_id": "adb4eb9a-7326-481d-b5be-cdbed9cd52a4",
#     "system": {
#       "dialog_turn_counter": 2,
#       "_node_output_map": {
#         "node_1_1557998509182": {
#           "0": [
#             0
#           ]
#         },
#         "node_1_1558005092998": {
#           "0": [
#             0
#           ]
#         }
#       },
#       "dialog_request_counter": 2,
#       "initialized": True,
#       "dialog_stack": [
#         {
#           "dialog_node": "root"
#         }
#       ],
#       "branch_exited": True,
#       "branch_exited_reason": "completed"
#     }
#   }
# }
watsonResponse = {
    "intents": [
        {
            "dialogCounter": 1,
            "confidence": 0.6066648006439209,
            "name": "contactNoInfoVerification"
        },
        {
            "dialogCounter": 1,
            "confidence": 0.28639530539512636,
            "name": "General_Ending"
        },
        {
            "dialogCounter": 1,
            "confidence": 0.2794786334037781,
            "name": "General_Greetings"
        },
        {
            "dialogCounter": 1,
            "confidence": 0.25608987212181095,
            "name": "getAcoountNoInfo"
        },
        {
            "dialogCounter": 1,
            "confidence": 0.25197352170944215,
            "name": "connectToAgent"
        },
        {
            "dialogCounter": 1,
            "confidence": 0.2371679872274399,
            "name": "raiseTicket"
        },
        {
            "dialogCounter": 1,
            "confidence": 0.23607154786586762,
            "name": "autoPaymentStepsInfo"
        },
        {
            "dialogCounter": 1,
            "confidence": 0.23559070229530335,
            "name": "paymentDetailsInfo"
        },
        {
            "dialogCounter": 1,
            "confidence": 0.23459672033786774,
            "name": "lateFeesChargeInfo"
        },
        {
            "dialogCounter": 1,
            "confidence": 0.23026899695396424,
            "name": "ticketStatus"
        }
    ],
    "outputMessages": [
        {
            "raw": {
                "text": [
                    "contactNoInfoVerification"
                ],
                "log_messages": [],
                "nodes_visited": [
                    "node_1_1557811993643"
                ]
            },
            "message": "contactNoInfoVerification",
            "type": "output"
        }
    ],
    "relations": [],
    "entities": [
        {
            "confidence": 0.78849196434021,
            "name": "9759170235",
            "literalLength": 10,
            "dialogCounter": 1,
            "literal": "9759170235",
            "location": [
                19,
                29
            ],
            "type": "ContactInfo"
        }
    ],
    "botRequest": {
        "info": {
            "ConvSessionId": "1559199055169d113e70f99e3e23d22c5"
        },
        "context": {
            "conversationId": "",
            "topIntents": [],
            "contextElements": [
                {
                    "raw": {},
                    "intents": [],
                    "dialogCounter": 0,
                    "nodesVisited": [],
                    "relations": [],
                    "conversationId": "",
                    "entities": []
                }
            ],
            "entities": []
        },
        "message": {
            "raw": {},
            "message": "you can call me on 9759170235",
            "type": "input"
        },
        "templateBotTag": "",
        "rawInput": {
            "message": "you can call me on 9759170235",
            "tag": "",
            "data": {
                "dbName": "testing",
                "parentId": 0
            },
            "context": {}
        }
    },
    "context": {
        "conversationId": "831ad81f-dc8d-4745-b93a-da53d2c0dcfe",
        "topIntents": [
            {
                "dialogCounters": [
                    1
                ],
                "confidence": [
                    0.6066648006439209
                ],
                "lastLocDiff": 0,
                "name": "contactNoInfoVerification",
                "numOccurrence": 1
            }
        ],
        "contextElements": [
            {
                "intents": [],
                "entities": [],
                "dialogCounter": 0,
                "nodesVisited": [],
                "relations": [],
                "conversationId": "",
                "raw": {}
            },
            {
                "conversationId": "831ad81f-dc8d-4745-b93a-da53d2c0dcfe",
                "entities": [
                    {
                        "confidence": 0.78849196434021,
                        "name": "9759170235",
                        "literalLength": 10,
                        "dialogCounter": 1,
                        "literal": "9759170235",
                        "location": [
                            19,
                            29
                        ],
                        "type": "ContactInfo"
                    }
                ],
                "intents": [
                    {
                        "dialogCounter": 1,
                        "confidence": 0.6066648006439209,
                        "name": "contactNoInfoVerification"
                    },
                    {
                        "dialogCounter": 1,
                        "confidence": 0.28639530539512636,
                        "name": "General_Ending"
                    },
                    {
                        "dialogCounter": 1,
                        "confidence": 0.2794786334037781,
                        "name": "General_Greetings"
                    },
                    {
                        "dialogCounter": 1,
                        "confidence": 0.25608987212181095,
                        "name": "getAcoountNoInfo"
                    },
                    {
                        "dialogCounter": 1,
                        "confidence": 0.25197352170944215,
                        "name": "connectToAgent"
                    },
                    {
                        "dialogCounter": 1,
                        "confidence": 0.2371679872274399,
                        "name": "raiseTicket"
                    },
                    {
                        "dialogCounter": 1,
                        "confidence": 0.23607154786586762,
                        "name": "autoPaymentStepsInfo"
                    },
                    {
                        "dialogCounter": 1,
                        "confidence": 0.23559070229530335,
                        "name": "paymentDetailsInfo"
                    },
                    {
                        "dialogCounter": 1,
                        "confidence": 0.23459672033786774,
                        "name": "lateFeesChargeInfo"
                    },
                    {
                        "dialogCounter": 1,
                        "confidence": 0.23026899695396424,
                        "name": "ticketStatus"
                    }
                ],
                "dialogCounter": 1,
                "outputMessages": [
                    {
                        "raw": {
                            "text": [
                                "contactNoInfoVerification"
                            ],
                            "log_messages": [],
                            "nodes_visited": [
                                "node_1_1557811993643"
                            ]
                        },
                        "message": "contactNoInfoVerification",
                        "type": "output"
                    }
                ],
                "raw": {
                    "conversation_id": "831ad81f-dc8d-4745-b93a-da53d2c0dcfe",
                    "system": {
                        "dialog_stack": [
                            {
                                "dialog_node": "root"
                            }
                        ],
                        "dialog_request_counter": 1,
                        "dialog_turn_counter": 1,
                        "branch_exited": True,
                        "initialized": True,
                        "_node_output_map": {
                            "node_1_1557811993643": {
                                "0": [
                                    0
                                ]
                            }
                        },
                        "branch_exited_reason": "completed"
                    }
                },
                "nodesVisited": [
                    {
                        "description": "",
                        "name": "node_1_1557811993643",
                        "id": ""
                    }
                ],
                "inputMessage": {
                    "message": "you can call me on 9759170235",
                    "type": "input"
                },
                "relations": []
            }
        ],
        "entities": [
            {
                "confidence": 0.78849196434021,
                "name": "9759170235",
                "literalLength": 10,
                "dialogCounter": 1,
                "literal": "9759170235",
                "location": [
                    19,
                    29
                ],
                "type": "ContactInfo"
            }
        ]
    },
    "inputMessage": {
        "message": "you can call me on 9759170235",
        "type": "input"
    }
}




outputDict = {
    '#intent': 'greetings',
    '@service': 'telecom',
    '$billerid': '101',
    '%info' : {}
}


def wrapper(watsonResponse):
    dict = {}
    for intents in watsonResponse["context"]['topIntents']:
        if len(intents) > 0:
            dict["#intent"] = intents['name']
    for entities in watsonResponse['context']['entities']:
        entity = entities['type']
        if len(entities) > 0:
            dict['@' + str(entity)] = entities['name']
            #dict.setdefault('@'+str(entity), []).append(entities['name'])
    context = watsonResponse['context']
    for Key in context.keys():
        dict['$' + str(Key)] = context[Key]
    # del dict['$conversation_id']
    # del dict['$system']

    # dict['~custom_info'] = None
    return dict


wrapper(watsonResponse)