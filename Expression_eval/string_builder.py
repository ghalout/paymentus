outputDict = {
    '#intent': 'greetings',
    '@service': 'telecom',
    '$billerid': '101',
    '%amount': '2000',
    '%transcationId' : '1000amsdj'
}

stringformat = 'your bill amount is %amount and your transcation id is %transcationId, for @service with biller id $billerid'
exceptionOutput  = 'your bill amount is 2000 and your transcation id is 1000amsdj, for telecom with biller id 101'

def string_builder(stringformat,outputDict):
    for var in outputDict.keys():
        stringformat = stringformat.replace(var, outputDict[var])
    #print stringformat, "testing"

    return stringformat

output = string_builder(stringformat,outputDict)
assert output == exceptionOutput


