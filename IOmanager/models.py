# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpResponse
import json

from django.views.decorators.csrf import csrf_exempt
from PaxcelBotFramework.InputOutputManager.models import SimpleInputProcessor,OutputProcessorType1
from BotManager.models import BotManager
from DecisionLogicLayer.models import processBotResponse
from MessageFormationLayer.messageHandler import createMessage
from django.db import models
from PaxcelBotFramework.DataAccessLayer.chatlogsHandler import dbChatlogHandler
import logging
logger = logging.getLogger(__name__)
import datetime
from dumper import dump


# Create your models here.
@csrf_exempt
def processInputRequest(request):
    
    """

    This is the first function called after a request is made

    :param request: request object
    :return: an http response


    """

    ##
    ## Initialise the Input Processor Object
    ##

    requestProcessor=SimpleInputProcessor()
    requestProcessor.extractRequestData(request)
    botRequest = requestProcessor.createBotRequest()
    responseType = requestProcessor.responseType
    if requestProcessor.requestType == "message":
        botResponse = BotManager.getProcessedNLPBotResponse(botRequest)
        decisionLogicResponse = processBotResponse(botResponse)

        ## Message Layer Response
        messageLayerResponse = createMessage(decisionLogicResponse)

        ## Output Processor Response
        outputDict = paymentUsOutputProcessorType1(outputContent=messageLayerResponse,
                                                   responseType=responseType).processRequest()

        # ConversationId= botResponse["botRequest"]["info"]['ConvSessionId']
        # logger.info(
        #     'ConversationId : {0}'.format((ConversationId)),
        #     extra={'ConversationId': botResponse["botRequest"]["info"]['ConvSessionId']
        #            }
        # )

    elif  requestProcessor.requestType == "userForm":
        outputDict = {
            "userForm":"Thanks for sharing your details"
        }
    elif  requestProcessor.requestType == "userForm":
        outputDict = {
            "userForm":"Thanks for sharing your details"
        }

    elif  requestProcessor.requestType == "credit":
        outputDict = {
            "userForm":"Thanks. Your payments has been made successfully"
        }



    # infoField = botResponse["botRequest"]["info"]
    #
    # logger.info(
    #     "User Query: {0}".format((userMessage)),
    #     extra={'ConversationId': botResponse["botRequest"]["info"]['ConvSessionId']   }
    # )

    ## decision Logic layer


    #print outputDict, "&&&&&&&&TEsting&&&&&&&&&&"

    return HttpResponse(json.dumps(outputDict, sort_keys=False, indent=4,separators=(',', ': ')), content_type = 'application/json', charset = 'UTF-8')



class paymentUsOutputProcessorType1(OutputProcessorType1):

    def __init__(self,outputContent=None,botDisplayName="",responseType="json", uiComponentTypeFunctionMap=None):

        uiComponentTypeFunctionMap={

            'APIDATA': paymentUsOutputProcessorType1.APIDATA,
        }

        super(paymentUsOutputProcessorType1, self).__init__(outputContent=outputContent,botDisplayName=botDisplayName,
                                                           responseType=responseType,uiComponentTypeFunctionMap=uiComponentTypeFunctionMap)


    def APIDATA(self, uiComponent):
        """

        This function loads the bot brain into the outputDict, returns modified output dict with the bot brain added .

        :param outputDict: A dictionary object , similar to the output structure
        :param responseObject: MessageLayerModel object which contains the response to the front end
        :return: modified outputDict (with the bot brain added)

        """

        uiComponent.pop("message", None)
        uiComponent.pop("tag", None)
        uiComponent.pop("name", None)
        uiComponent.pop("componentType", None)

        if self.responseType == "json":
            if "APIDATA" in self.outputDict.keys():
                # if self.responseType=="json"
                self.outputDict["APIDATA"].append(uiComponent)
            else:
                self.outputDict["APIDATA"] = [uiComponent]



