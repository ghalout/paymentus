# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from PaxcelBotFramework.InputOutputManager.models import SimpleInputProcessor,OutputProcessorType1
import json
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from django.conf import settings
from BotManager.models import BotManager
from DecisionLogicLayer.models import processBotResponse
from MessageFormationLayer.messageHandler import createMessage
import logging
logger = logging.getLogger(__name__)
from json2html import *
import os
import json
from DataAccessLayer.dbHandler import getWhtasAppChatHistory

import datetime









@csrf_exempt
def processInputRequestWhatsapp(request):

    """

    This is the first function called after a request is made

    :param request: request object
    :return: an http response


    """

    ##
    ## Initialise the Input Processor Object
    ##

    data = eval(request.POST['data'])

    print "incominhg start->", data
    number = data['from']
    toNumber = data['to']
    now = datetime.datetime.now()
    if 'text' in data.keys():
        msg = data['text']
    responsedata = ''
    result = getWhtasAppChatHistory(number)
    length = len(result)
    if length >= 1:
        if data['event'] == 'INBOX':
            text = data['text']
            fromNumber = data['from']
            request = {

                "input": {
                    "message": text,
                    "context": {},
                    "data": {},
                    "tag": ""

                },
                "info": {"name": 'whatsapp user', "emailId": '', 'phoneNo': fromNumber},
                "requestType": "message",
                "responseType": "json"

            }

            requestProcessor = SimpleInputProcessor()
            requestProcessor.extractRequestDataforFbBot(request)
            botRequest = requestProcessor.createBotRequest()
            responseType = requestProcessor.responseType
            if requestProcessor.requestType == "message":
                botResponse = BotManager.getProcessedNLPBotResponse(botRequest)
            else:
                botResponse = BotManager.getTemplateBotResponse(botRequest)

            userMessage = botRequest['message']["message"]

            logger.info(
                "User Query: {0}".format((userMessage)),
                extra={'ConversationId': botResponse["botRequest"]["info"]['ConvSessionId']}
            )

            ## decision Logic layer
            decisionLogicResponse = processBotResponse(botResponse)

            ## Message Layer Response
            messageLayerResponse = createMessage(decisionLogicResponse)

            ## Output Processor Response
            outputDict = paymentUsOutputProcessorType1(outputContent=messageLayerResponse,
                                              responseType=responseType).processRequest()
            #print outputDict['APIDATA'], "++++++++++++OUTPUT DICT+++++++++++"
            for x in outputDict['APIDATA']:
                for y in x['data']:
                    message = y['message']
            botReply = message

            ConversationId = botResponse["botRequest"]["info"]['ConvSessionId']
            logger.info(
                'ConversationId : {0}'.format((ConversationId)),
                extra={'ConversationId': botResponse["botRequest"]["info"]['ConvSessionId']
                       }
            )

            logger.info(
                "botReply: {0}".format((botReply)),
                extra={'ConversationId': botResponse["botRequest"]["info"]['ConvSessionId']
                       }
            )

            text1 = "".join(map(str, botReply))
            text2 = text1.replace("<br>", "")
            text3 = text2.replace("<b>", "")
            text4 = text3.replace("</b>", "")

            responsedata = {"apiwha_autoreply": text4}
    else:
        if data['event'] == 'INBOX':
            text = data['text']
            fromNumber = data['from']
            request = {

                "input": {
                    "message": text,
                    "context": {},
                    "data": {
                        "workspaceid" : "0b79b975-0499-4bad-a101-e2e88f3f6d92",
                        "dbName" : "i_i_Test3_10DB"
                    },
                    "tag": ""

                },
                "info": {"name": 'whatsapp user', "emailId": '', 'phoneNo': fromNumber},
                "requestType": "message",
                "responseType": "json"

            }

            requestProcessor = SimpleInputProcessor()
            requestProcessor.extractRequestDataforFbBot(request)
            botRequest = requestProcessor.createBotRequest()
            responseType = requestProcessor.responseType
            if requestProcessor.requestType == "message":
                botResponse = BotManager.getProcessedNLPBotResponse(botRequest)
            else:
                botResponse = BotManager.getTemplateBotResponse(botRequest)


            userMessage = botRequest['message']["message"]

            logger.info(
                "User Query: {0}".format((userMessage)),
                extra={'ConversationId': botResponse["botRequest"]["info"]['ConvSessionId']}
            )

            ## decision Logic layer
            decisionLogicResponse = processBotResponse(botResponse)

            ## Message Layer Response
            messageLayerResponse = createMessage(decisionLogicResponse)

            ## Output Processor Response
            outputDict = paymentUsOutputProcessorType1(outputContent=messageLayerResponse,
                                              responseType=responseType).processRequest()

            for x in outputDict['APIDATA']:
                for y in x['data']:
                    message = y['message']
            botReply = message


            ConversationId = botResponse["botRequest"]["info"]['ConvSessionId']
            logger.info(
                'ConversationId : {0}'.format((ConversationId)),
                extra={'ConversationId': botResponse["botRequest"]["info"]['ConvSessionId']
                       }
            )

            logger.info(
                "botReply: {0}".format((botReply)),
                extra={'ConversationId': botResponse["botRequest"]["info"]['ConvSessionId']
                       }
            )

            text1 = "".join(map(str, botReply))
            text2 = text1.replace("<br>", "")
            text3 = text2.replace("<b>", "")
            text4 = text3.replace("</b>", "")


            # text1 =str(botReply).replace("[", "").replace("]", "").replace(" ", "")
            responsedata = {"apiwha_autoreply": text4}

    return HttpResponse(json.dumps(responsedata, sort_keys=False, indent=4, separators=(',', ': ')),
                        content_type='application/json', charset='UTF-8')



class paymentUsOutputProcessorType1(OutputProcessorType1):

    def __init__(self,outputContent=None,botDisplayName="",responseType="json", uiComponentTypeFunctionMap=None):

        uiComponentTypeFunctionMap={

            'APIDATA': paymentUsOutputProcessorType1.APIDATA,
        }

        super(paymentUsOutputProcessorType1, self).__init__(outputContent=outputContent,botDisplayName=botDisplayName,
                                                           responseType=responseType,uiComponentTypeFunctionMap=uiComponentTypeFunctionMap)


    def APIDATA(self, uiComponent):
        """

        This function loads the bot brain into the outputDict, returns modified output dict with the bot brain added .

        :param outputDict: A dictionary object , similar to the output structure
        :param responseObject: MessageLayerModel object which contains the response to the front end
        :return: modified outputDict (with the bot brain added)

        """

        uiComponent.pop("message", None)
        uiComponent.pop("tag", None)
        uiComponent.pop("name", None)
        uiComponent.pop("componentType", None)

        if self.responseType == "json":
            if "APIDATA" in self.outputDict.keys():
                # if self.responseType=="json"
                self.outputDict["APIDATA"].append(uiComponent)
            else:
                self.outputDict["APIDATA"] = [uiComponent]