# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf import settings
import random
from SimpleMessageTemplates import MessageTemplates as SimpleMessageTemplates


SimpleMessageTemplates = SimpleMessageTemplates[settings.BOT_PERSONA]




def createMessage(uiComponentTrayFromDecisionLayer):


    updatedComponents = []

    componentProcessMap = {
        "APIDATA":processApiData,
        "SimpleMessage":processSimpleMessage

    }

    for component in uiComponentTrayFromDecisionLayer["uiComponents"]:
        #
        # based on component type select the specific processing function from component process map and
        # update the component
        #
        componentType = component["componentType"]
        updatedComponent = componentProcessMap[componentType](component)

        # append the updated components

        updatedComponents.append(updatedComponent)

    uiComponentTrayFromMessageLayer = {}
    uiComponentTrayFromMessageLayer["uiComponents"] = updatedComponents
    uiComponentTrayFromMessageLayer["botResponse"] = uiComponentTrayFromDecisionLayer["botResponse"]

    return uiComponentTrayFromMessageLayer


def processSimpleMessage(simpleMessageComponent):

    if simpleMessageComponent["tag"]=="createMessage.text":
        simpleMessageComponent["message"]=simpleMessageComponent["data"]

    return simpleMessageComponent


def processApiData(APIDATA):
    if APIDATA["tag"] == "createData.data":
        APIDATA["nextCallTag"] = "General_Greetings"
    elif APIDATA["tag"] == "createNLPmessage.data":
        APIDATA["nextCallTag"] = ""
    return APIDATA







